<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class, 'utama']);

Route::get('/register', [HomeController::class, 'reg']);

Route::post('/kirim', [HomeController::class, 'kirim']);

Route::get('/table', function(){
    return view('halaman.table');
});

Route::get('/data-table', function(){
    return view('halaman.data-table');
});

// CRUD Cast

// create
// form create cast
Route::get('/cast/create', [CastController::class, 'create']);

Route::post('/cast', [CastController::class, 'store']);

// read
// form read cast
Route::get('/cast', [CastController::class, 'index']);
// detail cast berdasarkan id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

// update
// form update cast
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
// update ke database berdasarkan id
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

// delete
// delete berdasarkan id
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);