@extends('layout.master')

@section('judul')
    Halaman Tambah Cast
@endsection

@section('content')

<form action="/cast" method="POST">
    @csrf
  <div class="form-group">
    <label>Nama Cast</label>
    <input type="text" name="nama" class="form-control">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="text" class="form-control" name="umur">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>Bio</label>
    <input type="text" class="form-control" name="bio">
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection